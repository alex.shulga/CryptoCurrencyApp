package com.ashulga.view;

import com.ashulga.constants.Constants;
import com.ashulga.controller.BitcoinCurrencyOperations;
import com.ashulga.controller.CryptoCurrencyOperations;
import com.ashulga.controller.Transport;

import java.util.Scanner;

public class CryptoCurrencyOperationsMenu {

    private Transport transport;

    public CryptoCurrencyOperationsMenu(Transport transport) {
        this.transport = transport;
    }

    public void input() {
        System.out.println("********** Welcome to CryptoCurrencyApp **********");

        showCryptoCurrencyOperations("Bitcoin", new BitcoinCurrencyOperations(transport));
    }

    private void showCryptoCurrencyOperations(String fullNameCurrency, CryptoCurrencyOperations cryptoCurrencyOperations) {
        Scanner scanner = new Scanner(System.in);
        String point;
        String result;
        while (true) {
            System.out.printf("----- The available operations -----\n" +
                    "1. Find out the current %s rate\n" +
                    "2. Find out the lowest/highest %s rate in the last %s days\n" +
                    "3. Exit the application\n" +
                    "Please enter the operation number:%n", fullNameCurrency, fullNameCurrency, Constants.PERIOD);

            point = scanner.next();
            switch (point) {
                case "1": {
                    System.out.println("Please enter the three-letter currency code. (USD, EUR, GBP, etc.)");
                    String currencyCode = validateAndGetThreeLettersCodeInCorrectForm(scanner.next());
                    if (currencyCode == null) {
                        System.out.println("You entered the wrong currency code, please try again");
                        break;
                    }
                    result = cryptoCurrencyOperations.getCurrentBitcoinRate(currencyCode);
                    System.out.println("********** Operation result **********");
                    System.out.println(result);
                    break;
                }
                case "2": {
                    System.out.println("Please enter the three-letter currency code. (USD, EUR, GBP, etc.)");
                    String currencyCode = validateAndGetThreeLettersCodeInCorrectForm(scanner.next());
                    if (currencyCode == null) {
                        System.out.println("You entered the wrong currency code, please try again");
                        break;
                    }
                    result = cryptoCurrencyOperations.getLowestHighestBitcoinRatesForPeriod(currencyCode, Constants.PERIOD);
                    System.out.println("********** Operation result **********");
                    System.out.println(result);
                    break;
                }
                case "3": {
                    System.out.println("********** Thank you. Goodbye **********");
                    System.exit(0);
                }
                default: {
                    System.out.println("You entered the wrong number, please try again");
                }
            }
        }
    }

    private String validateAndGetThreeLettersCodeInCorrectForm(String code) {
        if (code != null) {
            code = code.trim();
            if (Constants.PATTERN_TO_VALIDATE_THREE_LETTERS_CODE.matcher(code).matches()) {
                return code.toUpperCase();
            }
        }
        return null;
    }
}
