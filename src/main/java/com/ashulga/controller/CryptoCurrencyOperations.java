package com.ashulga.controller;

public interface CryptoCurrencyOperations {

    String getCurrentBitcoinRate(String currencyCode);

    String getLowestHighestBitcoinRatesForPeriod(String currencyCode, int period);

}
