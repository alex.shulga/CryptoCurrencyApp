package com.ashulga.controller;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Transport {

    private static final int DEFAULT_CONNECTION_TIMEOUT = 10000;

    public String sendGetRequest(String url) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            RequestConfig requestConfig = RequestConfig.custom()
                    .setSocketTimeout(DEFAULT_CONNECTION_TIMEOUT) // timeout until a connection with the server is established
                    .setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT) // timeout to receive data
                    .build();
            HttpGet httpGet = new HttpGet(url);
            httpGet.setConfig(requestConfig);

            HttpResponse httpResponse = httpClient.execute(httpGet);
            int status = httpResponse.getStatusLine().getStatusCode();
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                String responseString = EntityUtils.toString(entity);

                if (status == HttpStatus.SC_OK) {
                    return responseString;
                } else {
                    throw new IOException(String.format("Response code: %s, text: %s",
                            status, responseString));
                }
            } else {
                throw new IOException(String.format("Response code: %s, text: %s",
                        status, "Response entity is NULL"));
            }
        }
    }
}
