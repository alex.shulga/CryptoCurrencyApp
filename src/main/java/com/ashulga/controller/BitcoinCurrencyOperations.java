package com.ashulga.controller;

import com.ashulga.constants.Constants;
import com.ashulga.model.Bitcoin;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;

import static com.ashulga.constants.Constants.CURRENT_BITCOIN_RATE_URL;

public class BitcoinCurrencyOperations implements CryptoCurrencyOperations {

    private Transport transport;

    public BitcoinCurrencyOperations(Transport transport) {
        this.transport = transport;
    }

    @Override
    public String getCurrentBitcoinRate(String currencyCode) {
        String url = String.format(CURRENT_BITCOIN_RATE_URL, currencyCode);
        try {
            String answer = transport.sendGetRequest(url);
            if (answer == null) {
                return "Response is NULL";
            }
            JSONObject answerJSONObject = new JSONObject(answer);
            JSONObject bpiJSONObject = answerJSONObject.getJSONObject(Constants.KEY_BPI);
            JSONObject currencyJSONObject = bpiJSONObject.getJSONObject(currencyCode);

            Bitcoin bitcoin = new Bitcoin(
                    currencyJSONObject.getString(Constants.KEY_CODE),
                    currencyJSONObject.getString(Constants.KEY_DESCRIPTION),
                    currencyJSONObject.getBigDecimal(Constants.KEY_RATE)
            );
            return String.format("BTC/%s %s", bitcoin.getCurrencyCode(), bitcoin.getRate());
        } catch (IOException e) {
            return String.format("Failed to send GET request. %s.", e.getMessage());
        } catch (JSONException e) {
            return "Failed to parse response from server";
        }
    }

    public String prepareRequestParamsToGetBitcoinRatesForPeriod(String currencyCode, int period) {
        String paramsInRequest = "start=%s&end=%s&currency=%s";

        Calendar date = Calendar.getInstance();
        String endPeriod = Constants.DATE_FORMAT.format(date.getTime());
        date.add(Calendar.DATE, -period);
        String startPeriod = Constants.DATE_FORMAT.format(date.getTime());

        return String.format(paramsInRequest, startPeriod, endPeriod, currencyCode);
    }

    @Override
    public String getLowestHighestBitcoinRatesForPeriod(String currencyCode, int period) {
        String params = prepareRequestParamsToGetBitcoinRatesForPeriod(currencyCode, period);
        String url = String.join("?", Constants.BITCOIN_RATES_FOR_PERIOD_URL, params);
        try {
            String answer = transport.sendGetRequest(url);
            if (answer == null) {
                return "Response is NULL";
            }
            JSONObject answerJSONObject = new JSONObject(answer);
            JSONObject bpiJSONObject = answerJSONObject.getJSONObject(Constants.KEY_BPI);
            Set<String> records = bpiJSONObject.keySet();
            if (records.size() != period) {
                return String.format("Response doesn't contain all records for %s days. Observed %s records, Expected %s records",
                        period,
                        records.size(),
                        period
                );
            }

            LinkedList<Bitcoin> bitcoins = new LinkedList<>();
            for (String key : records) {
                Bitcoin bitcoin = new Bitcoin(
                        currencyCode,
                        bpiJSONObject.getBigDecimal(key),
                        key
                );
                bitcoins.add(bitcoin);
            }

            bitcoins.sort(Comparator.comparing(Bitcoin::getRate));

            Bitcoin lowestRateBitcoin = bitcoins.getFirst();
            Bitcoin highestRateBitcoin = bitcoins.getLast();

            String lowestRateBitcoinInfo = String.format("The lowest BTC/%s %s date %s",
                    lowestRateBitcoin.getCurrencyCode(), lowestRateBitcoin.getRate(), lowestRateBitcoin.getDate());

            String highestRateBitcoinInfo = String.format("The highest BTC/%s %s date %s",
                    highestRateBitcoin.getCurrencyCode(), highestRateBitcoin.getRate(), highestRateBitcoin.getDate());

            return String.join("\n", lowestRateBitcoinInfo, highestRateBitcoinInfo);
        } catch (IOException e) {
            return String.format("Failed to send GET request. %s.", e.getMessage());
        } catch (JSONException e) {
            return "Failed to parse response from server";
        }
    }
}
