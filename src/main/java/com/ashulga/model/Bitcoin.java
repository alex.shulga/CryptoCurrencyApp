package com.ashulga.model;

import java.math.BigDecimal;

public class Bitcoin {

    private String currencyCode;
    private String fullNameCurrency;
    private BigDecimal rate;
    private String date;

    public Bitcoin(String currencyCode, BigDecimal rate, String date) {
        this.currencyCode = currencyCode;
        this.rate = rate;
        this.date = date;
    }

    public Bitcoin(String currencyCode, String fullNameCurrency, BigDecimal rate) {
        this.currencyCode = currencyCode;
        this.fullNameCurrency = fullNameCurrency;
        this.rate = rate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getFullNameCurrency() {
        return fullNameCurrency;
    }

    public void setFullNameCurrency(String fullNameCurrency) {
        this.fullNameCurrency = fullNameCurrency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Bitcoin{" +
                "currencyCode='" + currencyCode + '\'' +
                ", fullNameCurrency='" + fullNameCurrency + '\'' +
                ", rate=" + rate +
                ", date='" + date + '\'' +
                '}';
    }
}
