package com.ashulga;

import com.ashulga.controller.Transport;
import com.ashulga.view.CryptoCurrencyOperationsMenu;

public class CryptoCurrencyApp {

    public static void main(String[] args) {
        Transport transport = new Transport();
        CryptoCurrencyOperationsMenu menu = new CryptoCurrencyOperationsMenu(transport);
        menu.input();
    }

}
