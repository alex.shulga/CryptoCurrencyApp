package com.ashulga.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class Constants {

    public static final Pattern PATTERN_TO_VALIDATE_THREE_LETTERS_CODE = Pattern.compile("^[a-zA-Z]{3}$");

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String CURRENT_BITCOIN_RATE_URL = "https://api.coindesk.com/v1/bpi/currentprice/%s.json";
    public static final String BITCOIN_RATES_FOR_PERIOD_URL =
            "https://api.coindesk.com/v1/bpi/historical/close.json";

    // response keys
    public static final String KEY_BPI = "bpi";
    public static final String KEY_CODE = "code";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_RATE = "rate_float";

    public static final int PERIOD = 30;

}
