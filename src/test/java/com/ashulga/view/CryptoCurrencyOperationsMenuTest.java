package com.ashulga.view;

import com.ashulga.controller.Transport;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class CryptoCurrencyOperationsMenuTest {

    private Transport transport;
    private CryptoCurrencyOperationsMenu operationsMenu;

    @Before
    public void setUp() {
        transport = Mockito.spy(Transport.class);
        operationsMenu = Mockito.spy(new CryptoCurrencyOperationsMenu(transport));
    }

    private static final List<String> CORRECT_THREE_LETTERS_CODES_LIST = Arrays.asList(
            "eur",
            "EUR",
            " eur",
            " eur ",
            "eur ",
            " EUR",
            " EUR ",
            "EUR ",
            "   EUR"
    );

    private static final List<String> INCORRECT_THREE_LETTERS_CODES_LIST = Arrays.asList(
            "EU1",
            "123",
            "\\",
            "???",
            "***",
            "...",
            "e",
            "eu",
            "euro",
            "eurEur"
    );

    @Test
    public void validateAndGetThreeLettersCodeInCorrectFormTest() throws Exception {
        assertNotNull(transport);
        assertNotNull(operationsMenu);

        String expectedResultForCorrectCodesList = "EUR";
        for (String inputData : CORRECT_THREE_LETTERS_CODES_LIST) {
            String actualResultForCorrectCodesList = callValidateAndGetThreeLettersCodeInCorrectFormPrivateMethod(inputData);
            // System.out.println(inputData + " --- " + actualResultForCorrectCodesList); // to get particular cases
            Assert.assertEquals(expectedResultForCorrectCodesList, actualResultForCorrectCodesList);
        }

        String expectedResultForIncorrectCodesList = null;
        for (String inputData : INCORRECT_THREE_LETTERS_CODES_LIST) {
            String actualResultForIncorrectCodesList = callValidateAndGetThreeLettersCodeInCorrectFormPrivateMethod(inputData);
            // System.out.println(inputData + " --- " + actualResultForIncorrectCodesList); // to get particular cases
            Assert.assertEquals(expectedResultForIncorrectCodesList, actualResultForIncorrectCodesList);
        }


    }

    private String callValidateAndGetThreeLettersCodeInCorrectFormPrivateMethod(String argument) throws Exception {
        return Whitebox.invokeMethod(
                operationsMenu,
                "validateAndGetThreeLettersCodeInCorrectForm",
                argument);
    }
}