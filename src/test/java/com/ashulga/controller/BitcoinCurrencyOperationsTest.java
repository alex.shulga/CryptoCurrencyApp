package com.ashulga.controller;

import com.ashulga.constants.Constants;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

public class BitcoinCurrencyOperationsTest {

    private Transport transport;
    private BitcoinCurrencyOperations bitcoinCurrencyOperations;

    @Before
    public void setUp() {
        transport = Mockito.spy(Transport.class);
        bitcoinCurrencyOperations = Mockito.spy(new BitcoinCurrencyOperations(transport));
    }

    @Test
    public void currentBitcoinRateSuccessResponseTest() throws IOException {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        String currencyCode = "eur".toUpperCase();
        String responseForEurCurrencyCode =
                "{\"bpi\":{" +
                        "\"USD\":{\"code\":\"USD\",\"rate\":\"60,369.7650\",\"description\":\"United States Dollar\",\"rate_float\":60369.765}," +
                        "\"EUR\":{\"code\":\"EUR\",\"rate\":\"50,670.2152\",\"description\":\"Euro\",\"rate_float\":50670.2152}" +
                        "}}";


        String url = String.format(Constants.CURRENT_BITCOIN_RATE_URL, currencyCode);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(responseForEurCurrencyCode);

        String actualResult = bitcoinCurrencyOperations.getCurrentBitcoinRate(currencyCode);
        String expectedResult = "BTC/EUR 50670.2152";

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void currentBitcoinRateResponseParseErrorTest() throws IOException {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        String currencyCode = "eur".toUpperCase();
        String incorrectRateValue = "qwerty";
        String responseForEurCurrencyCode =
                "{\"bpi\":{" +
                        "\"USD\":{\"code\":\"USD\",\"rate\":\"60,369.7650\",\"description\":\"United States Dollar\",\"rate_float\":60369.765}," +
                        "\"EUR\":{\"code\":\"EUR\",\"rate\":\"50,670.2152\",\"description\":\"Euro\",\"rate_float\":" + incorrectRateValue +  "}" +
                        "}}";
        String url = String.format(Constants.CURRENT_BITCOIN_RATE_URL, currencyCode);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(responseForEurCurrencyCode);

        String actualResult = bitcoinCurrencyOperations.getCurrentBitcoinRate(currencyCode);
        String expectedResult = "Failed to parse response from server";

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void currentBitcoinRateResponseNULLTest() throws IOException {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        String currencyCode = "eur".toUpperCase();
        String url = String.format(Constants.CURRENT_BITCOIN_RATE_URL, currencyCode);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(null);

        String actualResult = bitcoinCurrencyOperations.getCurrentBitcoinRate(currencyCode);
        String expectedResult = "Response is NULL";

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void lowestHighestBitcoinRatesForPeriodSuccessResponseTest() throws Exception {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        int period = 5;
        String currencyCode = "gbp".toUpperCase();
        String responseCurrencyRatesFor5Days =
                "{\"bpi\":" +
                        "{\"2021-04-07\":40729.5646," +
                        "\"2021-04-08\":42295.7735," +
                        "\"2021-04-09\":49396.7298," +
                        "\"2021-04-10\":43622.9941," +
                        "\"2021-04-11\":43777.3799}" +
                        "}";

        String params = "start=2021-04-07&end=2021-04-12&currency=" + currencyCode;
        String url = String.join("?", Constants.BITCOIN_RATES_FOR_PERIOD_URL, params);
        Mockito.when(bitcoinCurrencyOperations.prepareRequestParamsToGetBitcoinRatesForPeriod(currencyCode, period))
                .thenReturn(params);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(responseCurrencyRatesFor5Days);

        String actualResult = bitcoinCurrencyOperations.getLowestHighestBitcoinRatesForPeriod(currencyCode, period);

        String lowestRateBitcoinInfo = "The lowest BTC/GBP 40729.5646 date 2021-04-07";
        String highestRateBitcoinInfo = "The highest BTC/GBP 49396.7298 date 2021-04-09";
        String expectedResult = String.join("\n", lowestRateBitcoinInfo, highestRateBitcoinInfo);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void lowestHighestBitcoinRatesForPeriodResponseParseErrorTest() throws Exception {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        int period = 5;
        String currencyCode = "gbp".toUpperCase();
        String incorrectRateValue = "qwerty";
        String responseCurrencyRatesFor5Days =
                "{\"bpi\":" +
                        "{\"202112345-04-07\":40729.5646," +
                        "\"2021-04-08\":42295.7735," +
                        "\"2021-04-09\":49396.7298," +
                        "\"2021-04-10\":43622.9941," +
                        "\"2021-04-11\":" + incorrectRateValue + "}" +
                        "}";

        String params = "start=2021-04-07&end=2021-04-12&currency=" + currencyCode;
        String url = String.join("?", Constants.BITCOIN_RATES_FOR_PERIOD_URL, params);
        Mockito.when(bitcoinCurrencyOperations.prepareRequestParamsToGetBitcoinRatesForPeriod(currencyCode, period))
                .thenReturn(params);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(responseCurrencyRatesFor5Days);

        String actualResult = bitcoinCurrencyOperations.getLowestHighestBitcoinRatesForPeriod(currencyCode, period);
        String expectedResult = "Failed to parse response from server";

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void lowestHighestBitcoinRatesForPeriodWrongNumberOfRecordsTest() throws Exception {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        int period = 5;
        String currencyCode = "gbp".toUpperCase();
        String responseCurrencyRatesFor4Days =
                "{\"bpi\":" +
                        "{\"202112345-04-07\":40729.5646," +
                        "\"2021-04-08\":42295.7735," +
                        "\"2021-04-09\":49396.7298," +
                        "\"2021-04-10\":43622.9941}" +
                        "}";

        String params = "start=2021-04-07&end=2021-04-12&currency=" + currencyCode;
        String url = String.join("?", Constants.BITCOIN_RATES_FOR_PERIOD_URL, params);
        Mockito.when(bitcoinCurrencyOperations.prepareRequestParamsToGetBitcoinRatesForPeriod(currencyCode, period))
                .thenReturn(params);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(responseCurrencyRatesFor4Days);

        String actualResult = bitcoinCurrencyOperations.getLowestHighestBitcoinRatesForPeriod(currencyCode, period);
        String expectedResult = "Response doesn't contain all records for " + period + " days. Observed 4 records, Expected " + period + " records";

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void lowestHighestBitcoinRatesForPeriodResponseNULLTest() throws IOException {
        assertNotNull(transport);
        assertNotNull(bitcoinCurrencyOperations);

        int period = 5;
        String currencyCode = "gbp".toUpperCase();

        String params = "start=2021-04-07&end=2021-04-12&currency=" + currencyCode;
        String url = String.join("?", Constants.BITCOIN_RATES_FOR_PERIOD_URL, params);
        Mockito.when(bitcoinCurrencyOperations.prepareRequestParamsToGetBitcoinRatesForPeriod(currencyCode, period))
                .thenReturn(params);
        Mockito.when(transport.sendGetRequest(url)).thenReturn(null);

        String actualResult = bitcoinCurrencyOperations.getLowestHighestBitcoinRatesForPeriod(currencyCode, period);
        String expectedResult = "Response is NULL";

        assertEquals(expectedResult, actualResult);
    }

}